@extends('layout.layout')

@section('content')
            <h1>Show: {{ $task->title }}</h1>

    <div>
        <p>
            <strong>Title:</strong> {{ $task->title }}<br>
            <strong>Description:</strong> {{ $task->description }}
        </p>
    </div>
@endsection